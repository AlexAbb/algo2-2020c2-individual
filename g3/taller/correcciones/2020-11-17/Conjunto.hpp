
template <class T>
Conjunto<T>::Conjunto() : _raiz(), _card(0){
}

template<class T>
void Conjunto<T>::destruirNodo(Nodo* nod){
    if(nod != nullptr){
        destruirNodo(nod->izq);
        destruirNodo(nod->der);
        delete(nod);
        nod = nullptr;
    }
}

template <class T>
Conjunto<T>::~Conjunto() {
    destruirNodo(_raiz);
}

template<class T>
bool Conjunto<T>::perteneceNodo(const T& clave, Nodo* nod) const {
    bool res = false;
    if (nod != nullptr){
        if (nod->valor == clave){
            res = true;
        } else {
            if(clave < nod->valor){
                res = perteneceNodo(clave, nod->izq);
            } else {
                res = perteneceNodo(clave, nod->der);
            }
        }
    }
    return res;
}


template <class T>
bool Conjunto<T>::pertenece(const T& clave) const {
    return perteneceNodo(clave, _raiz);
}

template<class T>
void Conjunto<T>::insertNodo(const T& clave, Nodo* nod){
    if (clave < nod->valor){
        if (nod->izq == nullptr){
            nod->izq = new Nodo(clave);
            (nod->izq)->padre = nod;
        } else {
            insertNodo(clave, nod->izq);
        }
    } else {
        if (nod->der == nullptr){
            nod->der = new Nodo(clave);
            (nod->der)->padre = nod;
        } else {
            insertNodo(clave, nod->der);
        }
    }
}

template <class T>
void Conjunto<T>::insertar(const T& clave) {
    if(_raiz == nullptr){
        _raiz= new Nodo(clave);
        _card++;
    } else {
        if (!pertenece(clave)) {
            insertNodo(clave, _raiz);
            _card++;
        }
    }
}

template<class T>
void Conjunto<T>::removerNodo(const T& clave, Nodo* nod){
    Nodo* nodoClave = nod;
    while (nodoClave->valor != clave){
        if(clave < nodoClave->valor){
            nodoClave = nodoClave->izq;
        } else {
            nodoClave = nodoClave->der;
        }
    }
    if(nodoClave->der == nullptr && nodoClave->izq == nullptr){
        if(nodoClave == _raiz){
            delete(nodoClave);
            _raiz = nullptr;
        } else {
            if((nodoClave->padre)->der == nodoClave){
                (nodoClave->padre)->der = nullptr;
            } else {
                (nodoClave->padre)->izq = nullptr;
            }
            delete(nodoClave);
        }
    } else if (nodoClave->der == nullptr){
        (nodoClave->izq)->padre = nodoClave->padre;
        if(nodoClave == _raiz){
            _raiz = nodoClave->izq;
        } else {
            if((nodoClave->padre)->izq == nodoClave){
                (nodoClave->padre)->izq = nodoClave->izq;
            } else {
                (nodoClave->padre)->der = nodoClave->izq;
            }
        }
        delete(nodoClave);
    } else if (nodoClave->izq == nullptr){
        (nodoClave->der)->padre = nodoClave->padre;
        if (nodoClave == _raiz){
            _raiz = nodoClave->der;
        } else {
            if((nodoClave->padre)->der == nodoClave){
                (nodoClave->padre)->der = nodoClave->der;
            } else {
                (nodoClave->padre)->izq = nodoClave->der;
            }
        }
        delete(nodoClave);
    } else {
        T sig = this->siguiente(clave);
        nodoClave->valor = sig;
        removerNodo(sig, nodoClave->der);
    }
}

template <class T>
void Conjunto<T>::remover(const T& clave) {
    if (this->pertenece(clave)){
        removerNodo(clave, _raiz);
        _card--;
    }
}

template <class T>
const T& Conjunto<T>::siguiente(const T& clave) {
    Nodo* nodRes = nullptr;
    Nodo* nodoClave = _raiz;
    while (nodoClave->valor != clave){
        if(clave < nodoClave->valor){
            nodoClave = nodoClave->izq;
        } else {
            nodoClave = nodoClave->der;
        }
    }
    if (nodoClave->der != nullptr){
        nodRes = nodoClave->der;
        while(nodRes->izq != nullptr){
            nodRes = nodRes->izq;
        }
    } else {
        nodRes = nodoClave->padre;
        while(nodRes->der == nodoClave){
            nodoClave = nodRes;
            nodRes = nodoClave->padre;
        }
    }
    return nodRes->valor;
}

template <class T>
const T& Conjunto<T>::minimo() const {
    Nodo* nod = _raiz;
    while(nod->izq != nullptr){
        nod = nod->izq;
    }
    return nod->valor;
}

template <class T>
const T& Conjunto<T>::maximo() const {
    Nodo* nod = _raiz;
    while(nod->der != nullptr){
        nod = nod->der;
    }
    return nod->valor;
}

template <class T>
unsigned int Conjunto<T>::cardinal() const {
    return _card;
}

template <class T>
void Conjunto<T>::mostrar(std::ostream&) const {
    assert(false);
}

template<class T>
Conjunto<T>::Nodo::Nodo(const T& v) : valor(v), izq(), der(), padre(){

}
