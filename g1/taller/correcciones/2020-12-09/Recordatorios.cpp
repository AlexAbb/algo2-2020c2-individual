#include <iostream>
#include <list>
#include <map>

using namespace std;

using uint = unsigned int;

// Pre: 0 <= mes < 12
uint dias_en_mes(uint mes) {
    uint dias[] = {
        // ene, feb, mar, abr, may, jun
        31, 28, 31, 30, 31, 30,
        // jul, ago, sep, oct, nov, dic
        31, 31, 30, 31, 30, 31
    };
    return dias[mes - 1];
}

// Ejercicio 7, 8, 9 y 10

// Clase Fecha
class Fecha {
  public:
    // Completar declaraciones funciones
    Fecha(int mes, int dia);
    int mes();
    int dia();
    void incrementar_dia();

    #if EJ >= 9 // Para ejercicio 9
    bool operator==(Fecha o);
    #endif
    bool operator<(Fecha f);

  private:
    //Completar miembros internos
    int mes_;
    int dia_;
};

Fecha::Fecha(int mes, int dia) : mes_(mes), dia_(dia) {}

int Fecha::dia() {
    return dia_;
}

int Fecha::mes() {
    return mes_;
}

ostream& operator<<(ostream& os, Fecha f){
    os << f.dia() << "/" << f.mes();
    return os;
}

#if EJ >= 9
bool Fecha::operator==(Fecha o) {
    bool igual_dia = this->dia() == o.dia();
    // Completar iguadad (ej 9)
    bool igual_mes = this->mes_ == o.mes();
    return igual_dia & igual_mes;
}
#endif

void Fecha::incrementar_dia() {
    if(dia_ < dias_en_mes(mes_)){
        dia_++;
    } else {
        mes_++;
        dia_=1;
    }
}

// Ejercicio 11, 12


// Clase Horario

class Horario {
public:
    Horario(uint hora, uint min);
    uint hora();
    uint min();
    bool operator<(Horario h);

private:
    uint hora_;
    uint min_;
};

Horario::Horario(uint hora, uint min) : hora_(hora), min_(min) {};

uint Horario::hora() {
    return hora_;
}

uint Horario::min() {
    return min_;
}

ostream& operator<<(ostream& os, Horario h) {
    os << h.hora() << ":" << h.min();
    return os;
}

bool Horario::operator<(Horario h){
    bool res;
    if (hora_<h.hora()){
        res = true;
    } else if (hora_==h.hora()){
        if (min_<h.min()){
            res = true;
        } else {
            res = false;
        }
    } else {
        res = false;
    }
    return res;
}

// Ejercicio 13

// Clase Recordatorio

class Recordatorio{
public:
    Recordatorio(Fecha f, Horario h, string s);
    string mensaje();
    Fecha fecha();
    Horario hora();
private:
    string mensaje_;
    Fecha fecha_;
    Horario hora_;
};

Recordatorio::Recordatorio(Fecha f, Horario h, string s) : mensaje_(s), fecha_(f), hora_(h) {}

string Recordatorio::mensaje() {
    return mensaje_;
}

Fecha Recordatorio::fecha() {
    return fecha_;
}

Horario Recordatorio::hora() {
    return hora_;
}

ostream& operator<<(ostream& os, Recordatorio r){
    os << r.mensaje() << " @ " << r.fecha() << " " << r.hora();
    return os;
}

// Ejercicio 14

// Clase Agenda

class Agenda{
public:
    Agenda(Fecha fecha_inicial);
    void agregar_recordatorio(Recordatorio rec);
    void incrementar_dia();
    list<Recordatorio> recordatorios_de_hoy();
    Fecha hoy();

private:
    map<pair<int, int>, list<Recordatorio>> agenda_;
    Fecha fecha_act_;
};

Agenda::Agenda(Fecha fecha_inicial) : agenda_(), fecha_act_(fecha_inicial){};

void Agenda::incrementar_dia() {
    fecha_act_.incrementar_dia();
}

Fecha Agenda::hoy() {
    return fecha_act_;
}

void Agenda::agregar_recordatorio(Recordatorio rec) {
    Fecha fecha = rec.fecha();
    pair<int, int> f = make_pair(fecha.dia(), fecha.mes());
    if (agenda_.count(f)==0){
        list<Recordatorio> l;
        l.push_back(rec);
        agenda_[f]=l;
    } else {
        list<Recordatorio>::iterator it = agenda_[f].begin();
        while(it != agenda_[f].end() && it->hora()< rec.hora()){
            it++;
        }
        agenda_[f].insert(it, rec);
    }
}

list<Recordatorio> Agenda::recordatorios_de_hoy() {
    pair<int, int> f = make_pair(fecha_act_.dia(), fecha_act_.mes());
    return agenda_[f];
}

ostream& operator<<(ostream& os, Agenda a){
    os << a.hoy() << endl << "=====" << endl;
    for(Recordatorio r : a.recordatorios_de_hoy()){
        os << r << endl;
    }
    return os;
}
