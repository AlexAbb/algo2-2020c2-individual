template <typename T>
string_map<T>::string_map(): raiz(nullptr), _size(0){
}

template <typename T>
string_map<T>::string_map(const string_map<T>& aCopiar) : string_map() { *this = aCopiar; } // Provisto por la catedra: utiliza el operador asignacion para realizar la copia.

template<class T>
void string_map<T>::copiarNodo(Nodo*& nod ,Nodo* nod2){
    if(nod2 != nullptr){
        if(nod2->definicion != nullptr){
            nod = new Nodo(new T (*nod2->definicion));
        } else{
            nod = new Nodo();
        }
        for(int i = 0; i < 256; i++){
            if(nod2->siguientes[i] != nullptr){
                copiarNodo(nod->siguientes[i], nod2->siguientes[i]);
                nod->cantHijos = nod2->cantHijos;
            }
        }
    }
}

template <typename T>
string_map<T>& string_map<T>::operator=(const string_map<T>& d) {
    if(this->raiz != nullptr){
        borrarSiguientes(raiz);
        delete this->raiz;
        this->raiz= nullptr;
    }
    this->_size = d.size();
    copiarNodo(this->raiz, d.raiz);
    return *this;
}

template<typename T>
void string_map<T>::insert(const pair<string, T>& value_type){
    if(raiz == nullptr){
        if(value_type.first == ""){
            raiz = new Nodo (Nodo(new T(value_type.second)));
            raiz->cantHijos++;
            _size++;
        } else {
            raiz = new Nodo(Nodo());
            Nodo* nodoAct = raiz;
            for(int i = 0; i < value_type.first.size() - 1; i++){
                nodoAct->siguientes[int(value_type.first[i])] = new Nodo(Nodo());
                nodoAct->cantHijos++;
                nodoAct = nodoAct->siguientes[int(value_type.first[i])];
            }
            nodoAct->siguientes[int(value_type.first[value_type.first.size() - 1])] = new Nodo (Nodo(new T(value_type.second)));
            nodoAct->cantHijos++;
            _size++;
        }
    } else {
        Nodo* nodoAct = raiz;
        for(int i = 0; i < value_type.first.size() - 1; i++){
            if(nodoAct->siguientes[int(value_type.first[i])] == nullptr){
                nodoAct->siguientes[int(value_type.first[i])] = new Nodo(Nodo());
            }
            nodoAct->cantHijos++;
            nodoAct = nodoAct->siguientes[int(value_type.first[i])];
        }
        if (nodoAct->siguientes[int(value_type.first[value_type.first.size() - 1])] == nullptr){
            nodoAct->siguientes[int(value_type.first[value_type.first.size() - 1])] = new Nodo (Nodo(new T(value_type.second)));
            nodoAct->cantHijos++;
            _size++;
        } else {
            if((nodoAct->siguientes[int(value_type.first[value_type.first.size() - 1])])->definicion == nullptr){
                nodoAct->cantHijos++;
                _size++;
            } else {
                delete ((nodoAct->siguientes[int(value_type.first[value_type.first.size() - 1])])->definicion);
            }
            (nodoAct->siguientes[int(value_type.first[value_type.first.size() - 1])])->definicion = new T(value_type.second);
        }
    }

}

template <class T>
void string_map<T>::borrarSiguientes(Nodo* nod){
    if (nod != nullptr){
        for(int i = 0; i < 256; i ++){
            if(nod->definicion != nullptr){
                delete nod->definicion;
                nod->definicion = nullptr;
            }
            borrarSiguientes(nod->siguientes[i]);
            if(nod->siguientes[i] != nullptr){
                delete nod->siguientes[i];
                nod->siguientes[i] = nullptr;
            }
        }
    }
}

template <class T>
void string_map<T>::destruir() {
    if(raiz != nullptr){
        borrarSiguientes(raiz);
        delete raiz;
        raiz = nullptr;
    }
}

template <typename T>
string_map<T>::~string_map(){
    if(raiz != nullptr){
        borrarSiguientes(raiz);
        delete raiz;
        raiz = nullptr;
    }
}

template <typename T>
T& string_map<T>::operator[](const string& clave){
    return this->at(clave);
}


template <typename T>
int string_map<T>::count(const string& clave) const{
    int res = 0;
    Nodo* nodoAct = raiz;
    if(raiz != nullptr){
        for(int i = 0; i < clave.size(); i++){
            if(nodoAct->siguientes[int(clave[i])] == nullptr){
                break;
            }
            if(nodoAct != nullptr && i == clave.size() - 1 && (nodoAct->siguientes[int(clave[i])])->definicion != nullptr){
                res = 1;
            }
            nodoAct = nodoAct->siguientes[int(clave[i])];
        }
    }
    return res;
}

template <typename T>
const T& string_map<T>::at(const string& clave) const {
    Nodo* nodoAct = raiz;
    for(int i = 0; i < clave.size(); i++) {
        nodoAct = nodoAct->siguientes[int(clave[i])];
    }
    return *nodoAct->definicion;
}

template <typename T>
T& string_map<T>::at(const string& clave) {
    Nodo* nodoAct = raiz;
    for(int i = 0; i < clave.size(); i++) {
        nodoAct = nodoAct->siguientes[int(clave[i])];
    }
    return *nodoAct->definicion;
}

template <typename T>
void string_map<T>::erase(const string& clave) {
    Nodo* nodoAct = raiz;
    Nodo* ultSinBorrar = raiz;
    int iSinBorrar = 0;
    int i = 0;
    while(i < clave.size()) {
        nodoAct->cantHijos--;
        if (nodoAct->cantHijos > 0){
            ultSinBorrar = nodoAct;
            iSinBorrar = i;
        }
        nodoAct = nodoAct->siguientes[int(clave[i])];
        i++;
    }
    if (ultSinBorrar == raiz && raiz->cantHijos == 0){
        if(nodoAct != ultSinBorrar){
            while(ultSinBorrar->siguientes[int(clave[iSinBorrar])] != nodoAct){
                Nodo* aux = ultSinBorrar;
                ultSinBorrar = ultSinBorrar->siguientes[int(clave[iSinBorrar])];
                delete aux;
                iSinBorrar++;
            }
            delete ultSinBorrar;
            ultSinBorrar = nullptr;
            delete nodoAct->definicion;
            nodoAct->definicion = nullptr;
            delete nodoAct;
            nodoAct = nullptr;
            raiz = nullptr;
        } else {
            delete nodoAct->definicion;
            nodoAct->definicion = nullptr;
            delete nodoAct;
            raiz = nullptr;
        }
    } else {
        if(nodoAct->cantHijos != 0){
            delete nodoAct->definicion;
            nodoAct->definicion = nullptr;
        } else if (nodoAct == ultSinBorrar->siguientes[int(clave[iSinBorrar])]){
            delete nodoAct->definicion;
            nodoAct->definicion = nullptr;
            delete ultSinBorrar->siguientes[int(clave[iSinBorrar])];
            ultSinBorrar->siguientes[int(clave[iSinBorrar])] = nullptr;
        } else {
            Nodo* aux = ultSinBorrar;
            int iaux = iSinBorrar;
            ultSinBorrar = ultSinBorrar->siguientes[int(clave[iSinBorrar])];
            iSinBorrar++;
            while(ultSinBorrar->siguientes[int(clave[iSinBorrar])] != nodoAct){
                Nodo* aux2 = ultSinBorrar;
                ultSinBorrar = ultSinBorrar->siguientes[int(clave[iSinBorrar])];
                delete aux2;
                iSinBorrar++;
            }
            aux->siguientes[int(clave[iaux])] = nullptr;
            delete nodoAct->definicion;
            nodoAct->definicion = nullptr;
            delete ultSinBorrar->siguientes[int(clave[iSinBorrar])];
            delete ultSinBorrar;
        }
    }
    _size--;
}

template <typename T>
int string_map<T>::size() const{
    return this->_size;
}

template <typename T>
bool string_map<T>::empty() const{
    return raiz == nullptr;
}