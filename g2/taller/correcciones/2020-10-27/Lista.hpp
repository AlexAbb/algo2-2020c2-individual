#include "Lista.h"

Lista::Lista() : primero_(nullptr), ultimo_(nullptr), long_(0){
}

Lista::Lista(const Lista& l) : Lista() {
    //Inicializa una lista vacía y luego utiliza operator= para no duplicar el código de la copia de una lista.
    *this = l;
}

Lista::~Lista() {
    // Completar
    while(long_>0){
        this->eliminar(0);
    }
}

Lista& Lista::operator=(const Lista& aCopiar) {
    // Completar
    while(long_>0){
        this->eliminar(0);
    }
    for(int i = 0; i < aCopiar.long_; i++){
        this->agregarAtras(aCopiar.iesimo(i));
    }
    return *this;
}

void Lista::agregarAdelante(const int& elem) {
    Nodo* nodo = new Nodo(elem);
    nodo->siguiente = primero_;
    if(long_==0){
        ultimo_ = nodo;
    } else {
        primero_->anterior = nodo;
    }
    primero_ = nodo;
    long_++;
}

void Lista::agregarAtras(const int& elem) {
    Nodo* nodo = new Nodo(elem);
    nodo->anterior = ultimo_;
    if(long_==0){
        primero_ = nodo;
    } else {
        ultimo_->siguiente = nodo;
    }
    ultimo_ = nodo;
    long_++;
}

void Lista::eliminar(Nat i) {
    Nodo* nodoAct = primero_;
    if(i == 0){
        primero_ = nodoAct->siguiente;
    } else if (long_ - 1== i){
        nodoAct = ultimo_;
        ultimo_ = nodoAct->anterior;
    } else {
        for(Nat j = 0; j < i; j++){
            nodoAct = nodoAct->siguiente;
        }
        Nodo* nodoAnt = nodoAct->anterior;
        Nodo* nodoSig = nodoAct->siguiente;
        nodoAnt->siguiente = nodoSig;
        nodoSig->anterior = nodoAnt;
    }
    delete nodoAct;
    long_ --;
}

int Lista::longitud() const {
    // Completar
    return long_;
}

const int& Lista::iesimo(Nat i) const {
    // Completar
    Nodo* nodo = primero_;
    for(Nat j = 0; j < i; j++){
        nodo = nodo->siguiente;
    }
    return nodo->valor;
}

int& Lista::iesimo(Nat i) {
    // Completar (hint: es igual a la anterior...)
    Nodo* nodo = primero_;
    for(Nat j = 0; j < i; j++){
        nodo = nodo->siguiente;
    }
    return nodo->valor;
}

void Lista::mostrar(ostream& o) {
    // Completar
}

Lista::Nodo::Nodo(const int &e) : valor(e), siguiente(nullptr), anterior(nullptr){}
